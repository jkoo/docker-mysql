# MySQL server on Alpine Linux

The MySQL service image is created on a lightweight Linux distribution, [Alpine Linux](https://hub.docker.com/_/alpine/). It's a only 5MB from scratch. Total size is 160MB.

## Usage

```sh
docker create --name mysql \
  -v /path_to/mysql/db:/var/lib/mysql
  -v /path_to/mysql/conf:/etc/mysql/conf.d \
  -v /path_to/mysql/my.cnf:/etc/mysql/my.cnf \
  -p 3306:3306
  kukki/docker-mysql
docker start mysql
```

* /var/lib/mysql is the database repository
* /etc/mysql/conf.d is the configuration folder
* /etc/mysql/my.cnf is the MySQL configuration file

After it starts, a `admin` account is created. The password can be retrieved by running

```sh
docker logs mysql
```

You can specify the default admin password with `-e MYSQL_PASS=password` parameter in `docker create` command line.

## Issue tracking

If you find any issues, you are welcome to [report here](https://bitbucket.org/jkoo/docker-mysql/issues?status=new&status=open)

## License

- [GNU General Public License v3](http://www.gnu.org/licenses/gpl-3.0.en.html)
