#!/bin/sh

VOLUME_HOME="/var/lib/mysql"

chown -R mysql:mysql $VOLUME_HOME

if [[ ! -d $VOLUME_HOME/mysql ]]; then
    echo "=> An empty or uninitialized MySQL volume is detected in $VOLUME_HOME"
    echo "=> Installing MySQL ..."
    mysql_install_db
    echo "=> Done!"
    /initdb.sh
else
    echo "=> Using an existing volume of MySQL"
fi


# run this in the foreground so Docker won't exit
exec mysqld_safe
